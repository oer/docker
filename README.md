<!--- Local IspellDict: en -->
<!--- SPDX-FileCopyrightText: 2017-2020 Jens Lechtenbörger -->
<!--- SPDX-License-Identifier: CC0-1.0 -->

# Deprecated project
As of March 2020, this project is **deprecated**.  Docker images for
emacs-reveal are now built as part of the CI pipeline of
[emacs-reveal](https://gitlab.com/oer/emacs-reveal/).
See its [containter registry](https://gitlab.com/oer/emacs-reveal/container_registry).

# Old README follows
This project collects [Docker](https://www.docker.com/) images to
build [Open Educational Resources (OER)](https://en.wikipedia.org/wiki/Open_educational_resources)
based on the [free/libre and open source software (FLOSS)](https://en.wikipedia.org/wiki/Free_and_open-source_software)
bundle [emacs-reveal](https://gitlab.com/oer/emacs-reveal).
Notably, the images are used in
[GitLab Continuous Integration (CI)](https://docs.gitlab.com/ce/ci/)
environments to build presentations for [OER courses](https://oer.gitlab.io/).
Several images have been created over time, among which `emacs-reveal`
is current, while others do not receive updates any more (but are still
usable for older OER).

The docker image `emacs-reveal` is based on Debian GNU/Linux with GNU
Emacs (and several additional Emacs packages, in particular Org mode
and emacs-reveal with its dependencies), LaTeX, and
[reveal.js](https://revealjs.com/) with several plugins.  It provides
an environment to produce OER in the form of HTML presentations
generated from [Org mode](https://orgmode.org/) source files.
It also contains graphviz/dot, ditaa, and plantuml to be used with
[Babel](https://orgmode.org/worg/org-contrib/babel/).  The jar file
for plantuml is located at `/root/plantuml-1.2019.13/plantuml.jar`,
to which `org-plantuml-jar-path` needs to be set.  Also, function
`oer-reveal-publish-setq-defaults` activates languages listed in
`oer-reveal-publish-babel-languages`.

The [emacs-reveal howto](https://gitlab.com/oer/emacs-reveal-howto),
[its publication code publish.el](https://gitlab.com/oer/emacs-reveal-howto/blob/master/elisp/publish.el),
[and its CI pipeline configuration](https://gitlab.com/oer/emacs-reveal-howto/blob/master/.gitlab-ci.yml)
demonstrate a sample use for emacs-reveal and this Docker image.

# General notes

Docker images simplify installation of necessary software components,
reduce dependencies on external software repositories, and avoid
compatibility issues when different components are updated.  For
background information on Docker, see this
[Introduction to Docker](https://oer.gitlab.io/oer-courses/vm-neuland/04-Docker.html)
which was built using the Docker images provided here.

Build images as usual, e.g.:

```
docker build -t emacs-reveal -f emacs-reveal/Dockerfile .
```

Sample invocations:
- Start a shell from which to run `emacs`.
  Have the current directory bound to `/tmp/test` in the container.
  (This supposes that image `emacs-reveal` is available
  locally.  See next command for full URL.)

  `docker run --rm -it -v $PWD:/tmp/test emacs-reveal`
- Run Emacs in Docker container with GUI on GNU/Linux system.  The
  following command opens a shell, in which `emacs` can be started.
  Path `emacs-reveal-howto` could come from the
  [Howto on emacs-reveal](https://oer.gitlab.io/emacs-reveal-howto/howto.html).

  `docker run --rm -it --net=host -e DISPLAY -v $HOME/.Xauthority:/root/.Xauthority -v $PWD/emacs-reveal-howto:/tmp/emacs-reveal-howto registry.gitlab.com/oer/docker/emacs-reveal:4.1`

# Image `debian-emacs-tex`

This image is based on Debian Stretch (9.x) with GNU Emacs (24.5), Org
mode, and a LaTeX distribution to generate PDFs.

# Image `emacs-reveal`

This image is based on `debian-emacs-tex`.  It includes
`emacs-reveal` with its dependencies `org-re-reveal`, `org-re-reveal-ref`,
and `oer-reveal` as well as reveal.js with several of its plugins, cloned
from
[this GitLab repository](https://gitlab.com/oer/emacs-reveal-submodules).
Besides, it contains ditaa, ghostscript, graphviz/dot, imagemagick,
and plantuml for use with
[Babel](https://orgmode.org/worg/org-contrib/babel/).

**Note!** Versions of Docker images advance independently of version
numbers of the software bundle emacs-reveal.

## Image version 6.5.0

Update on 2020-03-06 to emacs-reveal 5.5.0

## Image version 6.4.0

Update on 2020-03-06 to emacs-reveal 5.4.0 and emacs-reveal-submodules 1.2.0

## Image version 6.3.0

Update on 2020-01-02 to emacs-reveal 5.3.0 and emacs-reveal-submodules 1.1.0

## Image version 6.2.0

Update on 2020-01-02 to emacs-reveal 5.2.0

## Image version 6.1.1

Update on 2020-01-02 to emacs-reveal 5.1.1

## Image version 6.1.0

Update on 2020-01-02 to emacs-reveal 5.1.0

## Image version 6.0.1

Update on 2019-12-31 to emacs-reveal 5.0.4

## Image version 6.0.0

Update on 2019-12-31 to emacs-reveal 5.0.3 with org 9.3.1 and
oer-reveal 2.0.2.

## Image version 5.14.0

Update on 2019-12-21 to emacs-reveal 4.4.0 with oer-reveal 1.15.0.

## Image version 5.13.1

Update on 2019-12-20 to debian:9.11-slim, plantuml-1.2019.13, and
emacs-reveal 4.3.0, which now includes elisp packages org (9.3),
org-re-reveal (2.12.0), org-re-reveal-ref (0.10.0), and oer-reveal
(1.14.0) as submodules.

## Image version 5.12.2

Update on 2019-10-21 to oer-reveal 1.13.2.

## Image version 5.12.1

Update on 2019-10-21 to oer-reveal 1.13.1.

## Image version 5.12.0

Update on 2019-10-20 to org-re-reveal 2.12.0 and oer-reveal 1.13.0.

## Image version 5.11.0

Update on 2019-10-17 to oer-reveal 1.12.0 with emacs-reveal-submodules 1.0.0.

## Image version 5.10.0

Update on 2019-10-16 to org-re-reveal 2.11.2 and oer-reveal 1.11.0.

## Image version 5.9.0

Update on 2019-10-14 to org-re-reveal 2.11.1, oer-reveal 1.10.0, and
emacs-reveal-submodules 0.11.0.

## Image version 5.8.0

Update on 2019-10-04 to oer-reveal 1.8.0.

## Image version 5.7.0

Update on 2019-09-29 to org-re-reveal 2.7.0, oer-reveal 1.7.0, and
emacs-reveal-submodules 0.10.2.

## Image version 5.6.0

Update on 2019-09-21 to org-re-reveal-ref 0.10.0.

## Image version 5.5.0

Include updated version of org-ref on 2019-09-18.

## Image version 5.4.1

Update on 2019-09-16 to org-re-reveal 2.5.1 and oer-reveal 1.6.1.

## Image version 5.4.0

Update on 2019-09-13 to org-re-reveal 2.5.0 and oer-reveal 1.6.0.

## Image version 5.3.0

Update on 2019-09-07 to Org mode version 9.2.6, org-re-reveal 2.4.0,
oer-reveal 1.4.0, emacs-reveal 4.1.0.

## Image version 5.2.0

Update on 2019-08-26 to new base image debian-emacs-tex with more
LaTeX packages.  Emacs-reveal now includes packages addressing
issue #2.  Also, update to oer-reveal 1.2.0.

## Image version 5.1.0

Update on 2019-08-23 to org-re-reveal 2.1.0 and oer-reveal 1.1.1.

## Image version 5.0.1

Update on 2019-08-21 to org-re-reveal 2.0.1.

## Image version 5.0

Update on 2019-08-21 to Org mode 9.2.5, emacs-reveal 4.0.0,
oer-reveal 1.0.0, and org-re-reveal 2.0.0.

## Image version 4.1

This version, created on 2019-08-01, adds plantuml and contains Org
mode version 9.2.3, emacs-reveal 3.0.2, emacs-reveal-submodules
0.9.6.1, oer-reveal 0.9.9.9.4, and org-re-reveal 1.1.7.

## Image version 4.0

Starting with image version 4.0, created on 2019-07-30, the Docker
image also contains graphviz and ditaa to generate figures from Org
sources.

This version contains Org mode version 9.2.3, emacs-reveal 3.0.2,
emacs-reveal-submodules 0.9.6.1, oer-reveal 0.9.9.9.4, and
org-re-reveal 1.1.6.

## Image version 3.4

Version 3.4, created on 2019-05-14, contains Org mode version 9.2.3,
emacs-reveal 3.0.2, emacs-reveal-submodules 0.9.6.1, oer-reveal
0.9.9.9.3, and org-re-reveal 1.1.5.

## Image version 3.3

Version 3.3, created on 2019-05-14, contains Org mode version 9.2.3,
emacs-reveal 3.0.2, emacs-reveal-submodules 0.9.6.1, oer-reveal
0.9.9.7, and org-re-reveal 1.1.5.

## Image version 3.2

Version 3.2, created on 2019-05-03, contains emacs-reveal 3.0.2 and
org-re-reveal 1.1.3.

## Image version 3.1

Version 3.1, created on 2019-04-12, contains emacs-reveal 3.0 and
org-re-reveal 1.0.4.

## Image version 3.0

Version 3.0, created on 2019-04-12, contains emacs-reveal 3.0.
That version describes three ways to use emacs-reveal in more detail.

## Image version 2.2

Version 2.2, created on 2019-04-11, contains oer-reveal 0.9.9.4.

## Image version 2.1

Version 2.1, created on 2019-04-08, contains oer-reveal 0.9.9.3 and
emacs-reveal-submodules 0.9.5 with further speed-ups when loading
presentations.

## Image version 2.0

Version 2.0, created on 2019-04-07, contains oer-reveal 0.9.9, which
uses data-src on img elements to speed up loading of presentations.

## Image version 1.6

Version 1.6, created on 2019-04-05, is an update to
oer-reveal 0.9.8.3 with emacs-reveal-submodules 0.9.2
(including updated versions of reveal.js and reveal.js-quiz).

## Image version 1.5

Version 1.5, created on 2019-04-05, properly supports German settings
with LaTeX export.  Inclusion of texlive-lang-german in underlying
image and update to oer-reveal 0.9.8.2.

## Image version 1.4

Version 1.4, created on 2019-04-04, is an update to oer-reveal 0.9.8.1
and emacs-reveal 1.0.2.

## Image version 1.3

Version 1.3, created on 2019-04-02, is an update to oer-reveal 0.9.6.

## Image version 1.2

Version 1.2, created on 2019-03-26, is an update to oer-reveal 0.9.5.

## Image version 1.1

Version 1.1, created on 2019-03-25, is an update of version 1.0 that
includes oer-reveal 0.9.3.

## Image version 1.0

Version 1.0, created on 2019-03-24, includes Org mode version 9.2.1
(9.2.1-23-g126a37-elpa @ /root/.emacs.d/elpa/org-9.2.2/),
org-re-reveal 1.0.3, org-re-reveal-ref 0.9.1, oer-reveal 0.9.2.


# Deprecated images

## Image `debian-emacs-tex-org`

This image is based on Debian Stretch (9.x) with GNU Emacs
(24.5), Org mode, and a LaTeX distribution to generate PDFs.

### Image version v3.4

Version 3.4, created on 2019-03-01, update of emacs-reveal-submodules
(0.9.1) and org-re-reveal (1.0.2).
This uses Org mode from ELPA (9.2.1-23-g126a37-elpa @ /root/.emacs.d/elpa/org-9.2.2/).

### Image version v3.3

Version 3.3, created on 2019-02-17, extends version 3.2 by including
reveal.js and some of its submodules.

### Image version v3.2

Version 3.2 is based on debian:9.7-slim (earlier images were based on
9.6-slim).

With version 3.2, created on 2019-02-13, MELPA packages
`org-re-reveal` (version 1.0.0) and `org-re-reveal-ref`
(version 0.9.1) are included in addition to Org mode (version 9.2.1).

Version 1.0.0 of org-re-reveal uses a new, customizable export key
binding.

### Image version v3.1

With version 3.1, created on 2019-02-10, MELPA packages
`org-re-reveal` (version 0.9.3) and `org-re-reveal-ref`
(version 0.9.1) are included in addition to Org mode (version 9.2.1).

### Image version v3.0

With version 3.0, created on 2019-02-09, MELPA packages
`org-re-reveal` (version 0.9.3) and `org-re-reveal-ref`
(version 0.9.0) are included in addition to Org mode (version 9.2.1).

### Image version v2.2

Version 2.2 contains the `master` branch of Org mode as of 2019-01-27,
which includes commit 102142b1a535c1100909f0ac2c68476265ac23cf, which
fixes another bug related to tables in LaTeX export.

### Image version v2.1

Version 2.1 contains the `master` branch of Org mode as of 2019-01-19
(commit c28eb3c2cb904666594e40c1dcae4437d954bde4), which fixes a bug
related to tables in LaTeX export.

### Image version v2.0

Version v2.0 contains the `next` branch of Org mode as of 2018-12-16
(commit 6920c4390d2ad604004a057c06ac8833486b3475), which contains a
keyword macro (since November 2017) and slightly enhanced
functionality for HTML export (since December 2018).

### Image version v1.0

Version v1.0 contains Org mode from MELPA, which is patched to include
a subtitle macro.

## Image `emacs-tex`

This image is based on `ubuntu:latest` (Ubuntu 16.04) with GNU Emacs
(24.5) and LaTeX environment to generate PDFs.  This was the
predecessor to debian-emacs-tex.  On the one hand I decided to switch
to Debian, on the other to get rid of the `latest` tag, which does not
carry helpful information.

## Image `emacs-tex-org`

This image is based on `emacs-tex` to include Emacs packages “org”,
“org-ref”, and “htmlize”.  Those packages are contained in
`archives.tar.gz`, created with the help of `emacs-tex`.

The most recent version of `emacs-tex-org` contains a patched version
of `ox.el` (of org-20171116) with unofficial support for a `subtitle`
macro.  See there for a proposed patch to org, of which only the
change to `ox.el` is included here:
https://lists.gnu.org/archive/html/emacs-orgmode/2017-11/msg00199.html
