#!/bin/sh
# Copyright (C) 2019 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0
#
# Clone repository and setup submodules
# This script expects three arguments:
# - Directory into which to clone the repository
# - URL of repository
# - Git tag/commit to checkout

if test -z "$3"
then
    echo "Supply installation path, repo URL, and git tag/commit!"
    exit 1
fi

mkdir -p "$1"
cd "$1"
git clone "$2"
BASE=$(basename "$2")
DIR=${BASE%.*}
cd "$DIR"
git checkout "$3"
git submodule sync --recursive
git submodule update --init --recursive
