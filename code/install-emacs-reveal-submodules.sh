#!/bin/sh
# Copyright (C) 2018-2019 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0
#
# Clone emacs-reveal-submodules with submodules.
# This script expects exactly one argument, the git tag/commit to checkout.

/tmp/install-with-submodules.sh "/root/.emacs.d" "https://gitlab.com/oer/emacs-reveal-submodules.git" "$1"
