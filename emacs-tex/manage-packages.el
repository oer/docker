;;; manage-packages.el --- Management of packages and their requirements
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; SPDX-FileCopyrightText: 2017 Jens Lechtenbörger
;; SPDX-License-Identifier: GPL-3.0-or-later

;;; License:
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.
;; If not, see http://www.gnu.org/licenses/ or write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:
;; Download packages and their requirements to files for later offline
;; installation, e.g.:
;; 1. (mp-download-pkgs '(org org-ref) "/tmp/archives")
;; This downloads org, org-ref, and their requirements to /tmp/archives.  Also,
;; it creates a file containing the names of downloaded files.
;; 2. (mp-install-pkgs "/tmp/archives")
;; This reads the names of downloaded files and installs them.

;;; Code:
(defvar mp-target-directory nil)

(defun mp-make-absname (basename)
  "Make absolute name for BASENAME under `mp-target-directory'."
  (concat mp-target-directory "/" basename))

(defun mp-archives ()
  "Make absolute name for file listing all archives."
  (mp-make-absname "archives.txt"))

(defun mp-geturl (pkg)
  "Create URL at which to download package PKG."
  (let* ((name (concat (package-desc-full-name pkg)
		       (package-desc-suffix pkg)))
	 (url (concat (package-archive-base pkg)
		      name)))
    (url-copy-file url (mp-make-absname name))
    name))

(defun mp-download-reqs (pkg-symbol)
  "Compute requirements for package PKG-SYMBOL."
  (let* ((pkg (cadr (assq pkg-symbol package-archive-contents)))
	 (reqs (package-compute-transaction (list pkg)
					    (package-desc-reqs pkg))))
    (mapcar 'mp-geturl reqs)))

(defun mp-write-archives (filelist)
  "Append names of files in FILELIST to `mp-archives', one per line."
  (let ((absnames (mapcar 'mp-make-absname filelist)))
    (write-region (concat (mapconcat 'identity absnames "\n") "\n") nil
		  (mp-archives) 'append)))

(defun mp-read-archives ()
  "Read names of files from `mp-archives', return as list."
  (with-temp-buffer
    (insert-file-contents (mp-archives))
    (split-string (buffer-string) "\n" t)))

(defun mp-download-pkgs (packages directory)
  "Download PACKAGES and their requirements to DIRECTORY.
Write list of downloaded files to `mp-archives'."
  (package-initialize)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
  (package-refresh-contents)
  (mkdir directory t)
  (let* ((mp-target-directory directory)
	 (filelist (mapcar 'mp-download-reqs packages)))
    (mapc 'mp-write-archives filelist)))

(defun mp-install-pkgs (directory)
  "Install packages mentioned in `mp-archives' from DIRECTORY."
  (package-initialize)
  (let* ((mp-target-directory directory)
	 (files (mp-read-archives)))
    (mapc 'package-install-file files)))

(provide 'manage-packages)
;;; manage-packages.el ends here
