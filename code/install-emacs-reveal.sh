#!/bin/sh
# Copyright (C) 2018-2019 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0
#
# Clone emacs-reveal with submodules.
# This script expects one argument, a Git tag/commit to checkout.

/tmp/install-with-submodules.sh "/root/.emacs.d/elpa" https://gitlab.com/oer/emacs-reveal.git "$1"
